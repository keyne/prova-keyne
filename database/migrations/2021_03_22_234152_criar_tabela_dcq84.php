<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriarTabelaDcq84 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DCQ84', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('MODEL');
            $table->string('FAT_PART_NO', 15)->unique();
            $table->integer('TOTAL_LOCATION');
            $table->foreign('MODEL')->references('id')->on('DCQMODEL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
