<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriarTabelaDcq841 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("DCQ841",function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('FAT_PART_NO');
            $table->string('PARTS_NO',15);
            $table->integer('UNT_USG');
            $table->longText('DESCRIPTION');
            $table->longText('REF_DESIGNATOR')->nullable();
            $table->timestamps();
            $table->foreign('FAT_PART_NO')->references('FAT_PART_NO')->on('DCQ84');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
