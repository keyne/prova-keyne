<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DcqModel extends Model
{
    use HasFactory;

    protected $table = 'DCQMODEL';
    public $timestamps = false;

    public function dcq84()
    {
        return $this->hasMany(Dcq84::class);
    }

}
