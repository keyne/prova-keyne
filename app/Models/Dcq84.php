<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dcq84 extends Model
{
    use HasFactory;

    protected $table = 'DCQ84';

    public function dcqmodel()
    {
        return $this->belongsTo(DcqModel::class);
    }
    public function dcq841()
    {
        return $this->hasMany(Dcq841::class);

    }

}
