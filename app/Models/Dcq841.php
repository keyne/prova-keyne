<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dcq841 extends Model
{
    use HasFactory;
    protected $table = 'DCQ841';

    public function dcq84()
    {
        return $this->belongsTo(Dcq84::class);
    }
}
